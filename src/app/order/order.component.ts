import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from '@angular/router';
import {RestService} from '../rest.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.component.html',
  styleUrls: ['./order.component.css']
})
export class OrderComponent implements OnInit {
  droppedItems: Array<any> = [];
  totalItems: number;
  itemsPerPage: number;
  orders: Array<any>;
  links: Array<any>;
  products: Array<Object>;

  constructor(
      private route: ActivatedRoute,
      private rest: RestService
  ) { }

  ngOnInit() {
    this.rest
        .getProducts()
        .subscribe((res: any) => this.renderProducts(res));

    this.rest
        .getOrders()
        .subscribe((res: any) => this.renderOrders(res));
  }

  renderOrders(res: any) {
    console.log(res);
    if (res === null || res.totalItems === 0) {
      return [];
    }
    this.itemsPerPage = res.itemsPerPage;
    this.totalItems = res.totalItems;
    this.links = res._links.item;
    this.orders = res._embedded.item;
  }

  renderProducts(res: any) {
    this.products = res._embedded.item;
  }

  onItemDrop(e: any) {
    // console.log('DroppedData: ', e.dragData);
    // console.log('DroppedItems: ', this.droppedItems);
    // console.log('Orders: ', this.orders);
    // Get the dropped data here
    this.droppedItems.push(e.dragData);
  }

  removeItem(order: any, list: Array<any>) {
    // console.log(order, list);
    const index = list.map(function (e) {
      return e.name
    }).indexOf(order.name);
    list.splice(index, 1);
  }

}
