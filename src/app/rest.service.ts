import {Injectable} from '@angular/core';
import {Headers, Http, RequestOptions} from '@angular/http';
import 'rxjs/Rx';
import {Observable} from 'rxjs/Observable';

@Injectable()
export class RestService {
    static BASE_URL = 'http://localhost:8000';
    private static JWT_TOKEN = 'eyJhbGciOiJSUzI1NiJ9.eyJyb2xlcyI6WyJST0xFX1NVUEVSX0FETUlOIl0sInVzZXJuYW1lIjoic3VwZXJhZG1pbiIsImlhdCI6MTUwMTEwNDUzNX0.hL4xe8MIcKz93NcAvvGnKBuI6t1xrKQBQLHL__PfYHwNYJVM-G6F0-C7uFGdY9ZpYkktV6N7RYae5tuPNYntocV_HCAv2JrNw--tOX1rFX7Y6vi9uBsvh1CYz8X4ehOrkaTqQlhx83YkfxAgcWYdeT4qYhIXtClTLaVen42mvOVsLJNERu8IitLDhcAGd-GyVhsYpPQuc5xiW2bg9l7Ikt_n4tkK-zsRqz2Ozc1kM7u6kRPiXK_Z1z2SMmFHItzL0nYp1_jW9yxUbvVA0Wu9AJwYnjeXo7fK7HrbAIAUIe5RmzRKmVcchLvfnSHPpv3VDr3KxJclMIXzQ79EGCVvgzfBHmYUVehQx_GlQw7PGUqDa_d-TrMgcaPEoJrFoVwQ3LjTjY0MyLx5QUajjrmhdd2CeRlBBBCwZHn8bDTnX2wE4Ww_SNFKaNND0DQ9w0mtxGtbDCSNS76bIeO2uwT1XujbKoweHfjsVFNUfBDjCfa9167H4rvOL9Wu_Nk5XlWYsXpj34rdFvxe6CqjP-ycLkDvRIZZx0L5zSV0Eu0r0TZW8CSzREFsFQnNWT_KP8gqp9DapJGta7IoL5CtGG_NT_vmixgifxWS7Tl9uCN7v_zHvjReu_9QR9E2iI7ZyJhwqpsYUwBEQDhLopvd0c789g_ijy1WZ2iibWk1u0gD4QA';

    constructor(private http: Http) {
    }

    getProducts(): Observable<any[]> {
        return this.query(`/products`);
    }

    getOrders(): Observable<any[]> {
        return this.query(`/orders`);
    }

    getProduct(id: string): Observable<any[]> {
        return this.query(`/products/${id}`);
    }

    getOrder(id: string): Observable<any[]> {
        return this.query(`/orders/${id}`);
    }

    private query(action: string, params?: Array<string>): Observable<any[]> {
        let queryURL = `${RestService.BASE_URL}${action}`;

        if (params) {
            queryURL = `${queryURL}?${params.join('&')}`;
        }

        const requestArgs = new RequestOptions();
        requestArgs.headers = new Headers(
            {
                'Authorization': `Bearer ${RestService.JWT_TOKEN}`
            }
        );

        return this.http.get(
            queryURL,
            requestArgs
        )
            .map((res: any) => res.json());
    }

}

export const REST_PROVIDER: Array<any> = [
    {provide: RestService, useClass: RestService}
];
