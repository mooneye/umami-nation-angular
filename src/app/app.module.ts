import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { Ng2DragDropModule } from 'ng2-drag-drop';

import {
  RouterModule,
  Routes
} from '@angular/router';

import {
  LocationStrategy,
  HashLocationStrategy,
  APP_BASE_HREF
} from '@angular/common';

import {REST_PROVIDER} from './rest.service';

import { AppComponent } from './app.component';
import { ProductComponent } from './product/product.component';
import { OrderComponent } from './order/order.component';

const routes: Routes = [
  // basic routes
  {path: '', redirectTo: 'orders', pathMatch: 'full'},
  {path: 'orders', component: OrderComponent},
  {path: 'products', component: ProductComponent},
];

@NgModule({
  declarations: [
    AppComponent,
    OrderComponent,
    ProductComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(routes), // <-- routes
    Ng2DragDropModule.forRoot()
  ],
  providers: [
    REST_PROVIDER,
    {provide: APP_BASE_HREF, useValue: '/'}
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
