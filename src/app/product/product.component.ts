import { Component, OnInit } from '@angular/core';
import {RestService} from '../rest.service';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-product',
  templateUrl: './product.component.html',
  styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {
    totalItems: number;
    itemsPerPage: number;
    products: Array<Object>;
    links: Array<Object>;

    constructor(
        private route: ActivatedRoute,
        private rest: RestService
    ) { }

    ngOnInit() {
        this.rest
            .getProducts()
            .subscribe((res: any) => this.renderProducts(res));
    }

    renderProducts(res: any) {
        console.log(res.totalItems, res.itemsPerPage);
        this.itemsPerPage = res.itemsPerPage;
        this.totalItems = res.totalItems;
        this.links = res._links.item;
        this.products = res._embedded.item;
    }
}
