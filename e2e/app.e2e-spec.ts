import { UmamiNationPage } from './app.po';

describe('umami-nation App', () => {
  let page: UmamiNationPage;

  beforeEach(() => {
    page = new UmamiNationPage();
  });

  it('should display welcome message', () => {
    page.navigateTo();
    expect(page.getParagraphText()).toEqual('Welcome to app!!');
  });
});
